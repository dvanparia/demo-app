import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {

  appPages: any;

  constructor() { }

  ngOnInit() {
    this.initPages();
  }

  initPages(): void {
    this.appPages = [
      {
        title : "Dashboard",
        url   : "/home/",
        icon  : "home"
      },
      {
        title : "Inbox",
        url   : "/inbox",
        icon  : "mail"
      },
      {
        title : "Archive",
        url   : "/archive",
        icon  : "archive"
      },
    ];
  }

}
